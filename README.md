# README #

### What is this repository for? ###

* This repository provides a new approach for building phamily groupings for the program Phamerator.

### How do I get set up? ###

* Refer to k_phamerate_instructions.pdf for installation and use instructions.

### Contribution guidelines ###

* Please contact if you have any contributions or suggestions.

### Who do I talk to? ###

* Contact Charles Bowman with any issues (cab106@pitt.edu)